import React, { Component } from "react";

class Employee extends Component {
  render() {
    return (
      <>
        <div className="col-sm-4 my-3">
          <div className="card text-left" style={{ width: "18rem" }}>
            <div className="card-body">
              <h5 className="card-title">{this.props.employee.nama}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {this.props.employee.position}
              </h6>
              <p className="card-text">
                {this.props.employee.tempat +
                  ", " +
                  this.props.employee.tanggal_lahir}
              </p>
              <hr />
              <p className="card-text">Rp. {this.props.employee.salary}</p>
              <span
                className="card-link text-danger font-weight-bold float-right btn"
                onClick={() => this.props.funDelete(this.props.employee.emp_id)}
              >
                Delete
              </span>
              <span className="card-link text-info font-weight-bold float-right btn"
                onClick={this.handleDetail}
              >
                Detail
              </span>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Employee;

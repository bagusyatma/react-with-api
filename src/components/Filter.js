import React, { Component } from "react";

class Filter extends Component {
  render() {
    return (
      <>
        <div className="form-row">
          <div className="input-group col-md mb-3">
            <input
              type="text"
              className="form-control"
              name="nama"
              onChange={this.props.name}
            />
          </div>
          <div className="input-group col-md mb-3">
            <input
              type="number"
              className="form-control"
              placeholder="Min"
              step="500000"
              name="min"
              onChange={this.props.min}
            />
            <input
              type="number"
              className="form-control"
              placeholder="Maks"
              step="500000"
              name="maks"
              onChange={this.props.maks}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.props.btnSalary}
              >
                Search
              </button>
            </div>
          </div>
          <div className="input-group col-md mb-3">
            <input type="text" name="position" onChange={this.props.position}/>
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.props.btnPosition}
              >
                Search
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Filter;

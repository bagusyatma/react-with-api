import React, { Component } from "react";
import { Link } from "react-router-dom";

class Nav extends Component {
  render() {
    return (
      <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <span className="navbar-brand">Employee</span>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav ml-auto text-left">
              <Link to="/">
                <span className="nav-item nav-link btn">Home</span>
              </Link>
              <Link to="/addEmployee">
                <span className="nav-item nav-link btn">AddEmployee</span>
              </Link>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Nav;

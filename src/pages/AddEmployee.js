import React, { Component } from "react";
import axios from "axios";

class addEmployee extends Component {
  state = {
    nama: "",
    tempat: "",
    tanggalLahir: "",
    jenisKelamin: "",
    agama: "",
    pekerjaan: "",
    alamat: "",
    position: "",
    salary: null,
  };

  handleChange = (e) => {
    if (e.target.name === "position") {
      if (e.target.value === "Bos") {
        this.setState({
          salary: 5000000,
        });
      } else if (e.target.value === "Karyawan") {
        this.setState({
          salary: 3000000,
        });
      }
    }

    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (
      this.state.nama === "" ||
      this.state.tempat === "" ||
      this.state.tanggalLahir === "" ||
      this.state.jenisKelamin === "" ||
      this.state.agama === "" ||
      this.state.pekerjaan === "" ||
      this.state.alamat === "" ||
      this.state.position === "" ||
      this.state.salary === ""
    ) {
      alert("Data tidak boleh ada yg kosong!");
    } else {
      const newTanggal = dateFormatter(this.state.tanggalLahir);

      const employee = {
        nama: this.state.nama,
        tempat: this.state.tempat,
        tanggal_lahir: newTanggal,
        jenis_kelamin: this.state.jenisKelamin,
        agama: this.state.agama,
        pekerjaan: this.state.pekerjaan,
        alamat: this.state.alamat,
        position: {
          position: this.state.position,
        },
        salary: {
          salary: this.state.salary,
        },
      };

      function dateFormatter(date) {
        const d = new Date(date);
  
        const ye = new Intl.DateTimeFormat("en", {
          year: "numeric",
        }).format(d);
        const mo = new Intl.DateTimeFormat("en", {
          month: "2-digit",
        }).format(d);
        const da = new Intl.DateTimeFormat("en", {
          day: "2-digit",
        }).format(d);
  
        return `${da}-${mo}-${ye}`;
      }
  
      axios
        .post(`http://localhost:7878/addEmployee`, { employee })
        .then((res) => {
          console.log(res);
          if (res.status === 200) {
            window.location.href = "/";
          } else {
            alert("Data gagal dimasukan!");
          }
        });
    }

    
  };

  render() {
    return (
      <>
        <div className="container text-left mt-3">
          <div className="form-group">
            <label htmlFor="nama">Nama</label>
            <input
              type="text"
              className="form-control"
              id="nama"
              name="nama"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <div className="row">
              <div className="col">
                <label htmlFor="tempat">Tempat Lahir</label>
                <input
                  type="text"
                  className="form-control"
                  id="tempat"
                  name="tempat"
                  onChange={this.handleChange}
                />
              </div>
              <div className="col">
                <label htmlFor="tanggal-lahir">Tanggal Lahir</label>
                <input
                  type="date"
                  className="form-control"
                  id="tanggal-lahir"
                  name="tanggalLahir"
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="jenis-kelamin">Jenis Kelamin</label>
            <select
              className="form-control"
              id="jenis-kelamin"
              name="jenisKelamin"
              onChange={this.handleChange}
            >
              <option></option>
              <option value="Pria">Pria</option>
              <option value="Wanita">Wanita</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="alamat">Alamat</label>
            <input
              type="text"
              className="form-control"
              id="alamat"
              name="alamat"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="agama">Agama</label>
            <select
              className="form-control"
              id="agama"
              name="agama"
              onChange={this.handleChange}
            >
              <option></option>
              <option value="Islam">Islam</option>
              <option value="Protestan">Protestan</option>
              <option value="Katolik">Katolik</option>
              <option value="Hindu">Hindu</option>
              <option value="Buddha">Buddha</option>
              <option value="Khonghucu">Khonghucu</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="pekerjaan">Pekerjaan</label>
            <input
              type="text"
              className="form-control"
              id="pekerjaan"
              name="pekerjaan"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="position">Posisi</label>
            <select
              className="form-control"
              id="position"
              name="position"
              onChange={this.handleChange}
            >
              <option></option>
              <option value="Bos">Bos</option>
              <option value="Karyawan">Karyawan</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="salary">Gaji</label>
            <input
              type="text"
              className="form-control"
              id="salary"
              name="salary"
              readOnly
              onChange={this.handleChange}
              value={this.state.salary}
            />
          </div>
          <br />
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.handleSubmit}
          >
            Submit
          </button>
        </div>
        <br />
      </>
    );
  }
}

export default addEmployee;

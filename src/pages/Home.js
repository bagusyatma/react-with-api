import React, { Component } from "react";
import axios from "axios";
import Employee from "./../components/Employee";
import Filter from "./../components/Filter";

class Home extends Component {
  state = {
    employees: [],
    edit: {
      employee: {
        emp_id: null,
        nama: "",
        tempat: "",
        tanggal_lahir: "",
        jenis_kelamin: "",
        agama: "",
        pekerjaan: "",
        alamat: "",
        position: {
          pos_id: null,
          position: "",
        },
        salary: {
          sal_id: null,
          salary: "",
        },
      },
    },
    nama: "",
    min: 0,
    maks: 0,
    position: "",
  };

  componentDidMount() {
    axios.get(`http://localhost:7878/getEmployees`).then((res) => {
      const employees = res.data;
      this.setState({ employees: employees, filterEmployees: employees });
    });
  }

  handleDelete = (id) => {
    axios.delete(`http://localhost:7878/deleteEmployee/` + id).then((res) => {
      console.log(res);
      console.log(res.data);
      window.location.reload();
    });
  };

  handleUpdate = () => {};

  nameHandler = (e) => {
    const data = this.state.employees;
    const theName = e.target.value.toLowerCase();
    const dataByName = data.filter((res) =>
      res.nama.toLowerCase().includes(theName)
    );
    this.setState({
      employees: dataByName,
    });
  };

  // findByName = () => {};

  minHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  maksHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  findBySalary = () => {
    const data = this.state.employees;
    const dataBySalary = data.filter(
      (res) => res.salary >= this.state.min && res.salary <= this.state.maks
    );

    this.setState({
      employees: dataBySalary,
    });
  };

  positionHandler = (e) => {
    e.preventDefault()
    console.log(e.target.value);
    
    this.setState({
      position: e.target.value,
    });
  };

  findByPosition = () => {
    console.log(this.state.position);

    // const data = this.state.employees;
    // const dataByPosition = data.filter((res) =>
    //   res.position.includes(this.state.position)
    // );

    // this.setState({
    //   employees: dataByPosition,
    // });
  };

  render() {
    const renderEmployees = this.state.employees.map((employee, i) => (
      <Employee
        key={i}
        employee={employee}
        funDelete={this.handleDelete}
        funUpdate={this.handleUpdate}
      />
    ));

    return (
      <>
        <div className="container-fluid mt-3">
          <Filter
            name={this.nameHandler}
            min={this.minHandler}
            maks={this.maksHandler}
            position={this.positionHandler}
            btnSalary={this.findBySalary}
            btnPosition={this.findByPosition}
          />
        </div>
        <div className="container">
          <div className="row">{renderEmployees}</div>
        </div>
      </>
    );
  }
}

export default Home;

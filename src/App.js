import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Nav from "./components/Nav";
import Home from "./pages/Home";
import addEmployee from "./pages/AddEmployee";

import "./App.css";

class App extends Component {
  state = {};
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Nav />
          <Route exact path="/" component={Home}></Route>
          <Route path="/addEmployee" component={addEmployee}></Route>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
